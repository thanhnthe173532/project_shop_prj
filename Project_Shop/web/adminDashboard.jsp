
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="./css/adminStyle.css">
</head>

<body>
    <div class="container">
        <div class="navigation">
            <ul>
                <li>
                    <a href="#">
                        <span class="icon"><ion-icon name="hammer-outline"></ion-icon></span>
                        <span class="title">ADMIN</span>
                    </a>
                </li>
                <li class="hovered">
                    <a href="admin-dashboard">
                        <span class="icon"><ion-icon name="home-outline"></ion-icon></span>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="admin-users">
                        <span class="icon"><ion-icon name="people-outline"></ion-icon></span>
                        <span class="title">Customers</span>
                    </a>
                </li>
                <li>
                    <a href="admin-products">
                        <span class="icon"><ion-icon name="bag-handle-outline"></ion-icon></span>
                        <span class="title">Products</span>
                    </a>
                </li>                
                <li>
                    <a href="admin-cates">
                        <span class="icon"><ion-icon name="file-tray-stacked-outline"></ion-icon></span>
                        <span class="title">Categories</span>
                    </a>
                </li>                
                <li>
                    <a href="index">
                        <span class="icon"><ion-icon name="log-out-outline"></ion-icon></span>
                        <span class="title">Back to Shop</span>
                    </a>
                </li>                
            </ul>
        </div>
    </div>

    <!-- main -->
    <div class="main">
        <div class="topbar">
            <div class="toggle">
                <ion-icon name="menu-outline"></ion-icon>
            </div>
            <!-- search -->
            <div class="search">
                <label>
                    <input type="text" placeholder="Search here">
                    <ion-icon name="search-outline"></ion-icon>
                </label>
            </div>
            <!-- UserImg -->
            <div class="user">
                <!-- <img src="./img/people/3.png" alt=""> -->
            </div>
        </div>

        <!-- Cards -->
        <div class="cardBox">
            <div class="card">
                <div class="">
                    <div class="numbers">${views}</div>
                    <div class="cardName">Total Views</div>
                </div>
                <div class="iconBx">
                    <ion-icon name="eye-outline"></ion-icon>
                </div>
            </div>
            <div class="card">
                <div class="">
                    <div class="numbers">${sold}</div>
                    <div class="cardName">Sales</div>
                </div>
                <div class="iconBx">
                    <ion-icon name="cart-outline"></ion-icon>
                </div>
            </div>
            <div class="card">
                <div class="">
                    <div class="numbers">${cuscount}</div>
                    <div class="cardName">Customers</div>
                </div>
                <div class="iconBx">
                    <ion-icon name="person-outline"></ion-icon>
                </div>
            </div>
            <div class="card">
                <div class="">
                    <div class="numbers">${procount}</div>
                    <div class="cardName">Products</div>
                </div>
                <div class="iconBx">
                    <ion-icon name="shirt-outline"></ion-icon>
                </div>
            </div>
        </div>

        <!-- order details list -->
        <div class="details">
            <div class="recentOrders">
                <div class="cardHeader">
                    <h2>Most Sold Product</h2>
                    <a href="admin-products" class="btn">View All</a>
                </div>

                <table>
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Category</td>
                            <td>Sold</td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${plist}" var="p">
                        <tr>
                            <td>${p.productName}</td>
                            <td>${p.unitPrice}</td>
                            <td>${p.categoryID}</td>
                            <td>${p.sellQuantity}</td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <!-- New customers -->
            <div class="recentCustomers">
                <div class="cardHeader">
                    <h2>Recent Customers</h2>
                </div>
                <ul>
                    <c:forEach items="${newcus}" var="ac">
                    <li>
                        <ion-icon name="person-circle-outline"></ion-icon>
                        <span>${ac.user.name}</span>
                    </li>                    
                    </c:forEach>
                </ul>
            </div>
        </div>

    </div>

    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

    <script>
        // menu toggle
        let toggle = document.querySelector('.toggle')
        let navigation = document.querySelector('.navigation')
        let main = document.querySelector('.main')

        toggle.addEventListener('click', function() {
            navigation.classList.toggle('active')
            main.classList.toggle('active')
        })

        // add hovered class in selected list item
        // let list = document.querySelectorAll('.navigation li')
        // function activeLink() {
        //     list.forEach((item) => item.classList.remove('hovered'));
        //     this.classList.add('hovered');            
        // }
        // list.forEach((item) => item.addEventListener('mouseover', activeLink));
    </script>
</body>

</html>
