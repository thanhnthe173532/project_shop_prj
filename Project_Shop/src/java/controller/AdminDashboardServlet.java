/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dal.AccountDAO;
import dal.CategoryDAO;
import dal.ProductDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.Category;
import model.Product;

/**
 *
 * @author thanh
 */
public class AdminDashboardServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 
        ProductDAO pdao = new ProductDAO();
        CategoryDAO cdao = new CategoryDAO();
        AccountDAO acdao = new AccountDAO();
        List<Product> plist = pdao.getListTopSaleProduct();
        ArrayList<Category> clist = cdao.getCategoryList();
        List<Account> acc = acdao.getNewestCustomer();
        int viewcount = pdao.getTotalViewsCount();
        int soldcount = pdao.getTotalSoldProduct();
        int proCount = pdao.getTotalProCount();
        int cusCount = acdao.getTotalCusCount();
        
        req.setAttribute("cuscount", cusCount);
        req.setAttribute("procount", proCount);
        req.setAttribute("newcus", acc);
        req.setAttribute("sold", soldcount);
        req.setAttribute("views", viewcount);
        req.setAttribute("plist", plist);
        req.getRequestDispatcher("adminDashboard.jsp").forward(req, resp);
    }
    
}
