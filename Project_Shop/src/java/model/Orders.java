/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Thanh
 */
public class Orders {
    int orderId;
    User user;
    String orderDate;
    String shippedDate;
    double shipCost;
    String shipName;
    String shipAdress;
    String shipCity;

    public Orders() {
    }

    public Orders(int orderId, User user, String orderDate, String shippedDate, double shipCost, String shipName, String shipAdress, String shipCity) {
        this.orderId = orderId;
        this.user = user;
        this.orderDate = orderDate;
        this.shippedDate = shippedDate;
        this.shipCost = shipCost;
        this.shipName = shipName;
        this.shipAdress = shipAdress;
        this.shipCity = shipCity;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(String shippedDate) {
        this.shippedDate = shippedDate;
    }

    public double getShipCost() {
        return shipCost;
    }

    public void setShipCost(double shipCost) {
        this.shipCost = shipCost;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipAdress() {
        return shipAdress;
    }

    public void setShipAdress(String shipAdress) {
        this.shipAdress = shipAdress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }
    
    
}
