-- create database
create database MyShop

-- create table
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](30) NULL,
	[Password] [nvarchar](30) NULL,
	[Phone] [varchar](20) NULL,
	[UserID] [int] NULL,
	[Role] [int] NULL,	
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](15) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [int] IDENTITY(1,1) NOT NULL,	
	[Name] [nvarchar](100) NOT NULL,		
	[Gender] [nvarchar](25) NULL,
	[BirthDate] [date] NULL,
	[CreateDate] [date] NULL,
	[Address] [nvarchar](60) NULL,
	[AccountId] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order Details](
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Discount] [real] NOT NULL,
 CONSTRAINT [PK_Order_Details] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,	
	[UserID] [int] NULL,
	[OrderDate] [datetime] NULL,	
	[ShippedDate] [datetime] NULL,
	[ShipCost] [money] NULL,
	[ShipName] [nvarchar](40) NULL,
	[ShipAddress] [nvarchar](60) NULL,
	[ShipCity] [nvarchar](15) NULL,	
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](40) NOT NULL,
	[CategoryID] [int] NULL,
	[QuantityPerUnit] [nvarchar](20) NULL,
	[UnitPrice] [money] NULL,
	[UnitsInStock] [smallint] NULL,	
	[Discontinued] [bit] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


-- config default value
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_UnitPrice]  DEFAULT ((0)) FOR [UnitPrice]
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_Quantity]  DEFAULT ((1)) FOR [Quantity]
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_Discount]  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_ShipCost]  DEFAULT ((0)) FOR [ShipCost]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_UnitPrice]  DEFAULT ((0)) FOR [UnitPrice]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_UnitsInStock]  DEFAULT ((0)) FOR [UnitsInStock]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_Discontinued]  DEFAULT ((0)) FOR [Discontinued]


-- add foreign key
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Accounts] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Accounts] ([AccountId]) on delete cascade on update cascade
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Accounts]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Order_Details_Orders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID]) on delete cascade on update cascade
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [FK_Order_Details_Orders]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Order_Details_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID]) on delete cascade on update cascade
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [FK_Order_Details_Products]
GO
ALTER TABLE [dbo].[Orders]  WITH NOCHECK ADD  CONSTRAINT [FK_Orders_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID]) on delete cascade on update cascade
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Users]
GO
ALTER TABLE [dbo].[Products]  WITH NOCHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Categories] ([CategoryID]) on delete cascade on update cascade

-- add unique
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [Unique_User] UNIQUE ([UserID])
ALTER TABLE [dbo].[Accounts] ADD CONSTRAINT [Unique_Email] UNIQUE ([Email])
ALTER TABLE [dbo].[Users] ADD CONSTRAINT [Unique_Account] UNIQUE ([AccountID])

-- add new column and new key
alter table [Products] add UserID int
alter table [Products] add [Description] nvarchar(max)
alter table [Products] add [Images] nvarchar(max)
alter table [Products] add [Type] nvarchar(100)
alter table [Products] add [views] int

GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Users]

alter table [Products] add [SellQuantity] int default 0

-- insert values
insert into Accounts(Email, Password, Phone, Role, UserID) values ('ntt2001@gmail.com', '123', '012345678', 1, 0)
insert into Users(Name, Gender,CreateDate,BirthDate,Address,AccountId) values ('Nguyen Tien Thanh', 'Male', '2023-06-15', '2002-12-27', 'Son La', 1)

insert into Categories(CategoryName, [Description]) values ('Shirt', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')
insert into Categories(CategoryName, [Description]) values ('Pan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')
insert into Categories(CategoryName, [Description]) values ('Other', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem.')

insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (1,'Hawaii T-Shirt', 1, '1', '123', 10, 0);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (2, 'lily T-Shirt', 1, '1', '123', 10, 0);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (3, 'Daisy T-Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (4, 'Sakura T-Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (5, 'Pink Hibiscus T-Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (6, 'Men Corduroy Shirt',1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (7,'Women Capri Pants', 2, '1','123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (8, 'Women Cartoon Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (9, 'Sky-blue melange Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (10, 'Everlane Blue Relaxed Linen Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (11, 'Muji Off White Shirt', 1, '1', '123', 10, 1);
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (12, 'G Star Bristum Shirt Green', 1, '1', '123', 10, 1 );
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (13, 'Fashion Biz Indie Mens Long Sleeve Shirt', 1, '1', '123', 10, 1 );
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (14, N'Hackett London Stripe Shorts', 2, '1', '123', 10, 1 );
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (15, 'Bonobo Jeans MIT Langen �RMELN Shirt Men', 1, '1', '123', 10, 1 );
insert into Products(ProductID, ProductName, CategoryID, QuantityPerUnit, UnitPrice, UnitsInStock, Discontinued) values (16, 'James Perse Male Shirt', 1, '1', '123', 10, 1 );



-- delete 
delete from Accounts
delete from Users

-- querry
select * from Accounts
select * from Users
select * from Categories
select * from Products 



